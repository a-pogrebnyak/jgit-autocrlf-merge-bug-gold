package bar.foo.dependent.second;

///@name Imports
//@{

import bar.foo.dependency.second.DependencySecond;

//@}

public final class DependentSecond
{
    ///@name Methods
    //@{

        public static void main ( final String[] args )
        {
            new DependencySecond( ).sayDependencySecond( );

            System.out.println( "I am dependent-second" );
        }

    //@}
}

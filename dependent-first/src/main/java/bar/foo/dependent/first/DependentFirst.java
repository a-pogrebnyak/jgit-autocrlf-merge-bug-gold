package bar.foo.dependent.first;

///@name Imports
//@{

import bar.foo.dependency.second.DependencySecond;

//@}

public final class DependentFirst
{
    ///@name Methods
    //@{

        public static void main ( final String[] args )
        {
            new DependencySecond( ).sayDependencySecond( );

            System.out.println( "I am dependent-first" );
        }

    //@}
}
